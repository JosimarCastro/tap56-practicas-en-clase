
package MiniCad;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

public class Texto extends Figura{
    
    Point punto1;
    String texto;
    
    Texto(String _texto,Point _punto1, Color _color){
        punto1 = new Point(_punto1.x,_punto1.y);
        texto = _texto;
        color = _color;    
    }

    Texto(String linea) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    void Dibujar(Graphics2D g2d){
        if(this.color!=null){
            g2d.setColor(color);
        }
        g2d.drawString(texto, punto1.x, punto1.y);
    }  
}
