
package MiniCad;

import java.awt.Color;
import java.awt.Graphics2D;

public class Triangulo extends Figura {
    
    Linea linea1;
    Linea linea2;
    Linea linea3;
    Color color;

    public Triangulo(Linea linea1, Linea linea2, Linea linea3, Color color) {
        this.linea1 = linea1;
        this.linea2 = linea2;
        this.linea3 = linea3;
        this.color = color;
    }

    Triangulo(String linea) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
                
    void Dibujar(Graphics2D g){
        if (this.color!=null){
            g.setColor(color);
            
        }        
        linea1.Dibujar(g);
        linea2.Dibujar(g);
        linea3.Dibujar(g);
        
        
   }
    
}
