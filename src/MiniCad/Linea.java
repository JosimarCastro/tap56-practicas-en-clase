
package MiniCad;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

public class Linea extends Figura{
      
    Point punto1;
    Point punto2;

    Linea(Point p1, Point p2, Color col){
        punto1 = new Point(p1.x,p1.y);
        punto2 = new Point(p2.x,p2.y);        
        color  = col;
    }

    Linea(String linea) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    void Dibujar(Graphics2D g){
        if (this.color!=null){
            g.setColor(color);
        }        
        g.drawLine(punto1.x,punto1.y,punto2.x,punto2.y);
   }
}
