
package MiniCad;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

public class Ovalo extends Figura{
    
    Point punto1;
    Point punto2;
    boolean relleno = false;

    public Ovalo(Point _punto1, Point _punto2, Color _color,boolean _relleno) {
        punto1 = new Point(_punto1.x,_punto1.y);
        punto2 = new Point(_punto2.x,_punto2.y);
        color = _color;   
        relleno  = _relleno;
    }

    Ovalo(String linea) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    void Dibujar(Graphics2D g2d){
        if(this.color!=null){
            g2d.setColor(color);
        }
        
        if(relleno==false)
        {        
            g2d.drawOval(punto1.x, punto1.y, Math.abs(this.punto2.x-this.punto1.x),Math.abs(this.punto2.y-this.punto1.y));
        }else{
            g2d.fillOval(punto1.x, punto1.y, Math.abs(this.punto2.x-this.punto1.x),Math.abs(this.punto2.y-this.punto1.y));
        }
    }
}
