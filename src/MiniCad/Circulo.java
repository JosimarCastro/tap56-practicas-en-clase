
package MiniCad;

import java.awt.Point;
import java.awt.Graphics2D;
import java.awt.Color;

public class Circulo extends Figura{
    
    Point centro;
    int radio;
    boolean relleno;
    
    Circulo(Point _centro, int _radio, Color _color, boolean _relleno){
        centro = new Point(_centro.x,_centro.y);
        radio = _radio;
        color = _color;
        relleno = _relleno;
        
    }

    Circulo(String linea) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    void Dibujar(Graphics2D g2d){
        if(this.color!=null){
            g2d.setColor(color);
        }
        if(relleno==true){
            g2d.fillOval(centro.x, centro.y, radio, radio);
        }else{
            g2d.drawOval(centro.x, centro.y, radio, radio);            
        }
    }
    
    
    
}
