package MiniCad;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;


public class PracticaNo8 extends javax.swing.JFrame {
    
    public final int Esperandopunto1 = 0;
    public final int Esperandopunto2 = 1;
    public final int Esperandopunto3 = 2;
    public final int Esperandopunto4 = 3;
    
    public final int Linea     = 1;
    public final int Triangulo = 2;
    public final int Circulo = 4;
    public final int Cuadrado = 3;
    public final int Texto = 6;
    public final int Ovalo = 5;

    Point punto1;
    Point punto2;
    Point punto3;
    Point punto4;
    boolean relleno = true;
    
    ArrayList<Figura> Figuras = new ArrayList<Figura>();
    
    int Estado  = Esperandopunto1;
    int Figura  = Linea;
    
    Color color = Color.BLACK;

    public PracticaNo8() {
        initComponents();
        this.punto1 = new Point();
        this.punto2 = new Point();
        this.punto3 = new Point();
        this.punto4 = new Point();
        this.tfColor.setBackground(color);
    }
    
    public void DibujarPunto(Point p, Color color){
        Graphics2D g= (Graphics2D) this.jPanel3.getGraphics();
        g.setColor(color);
        g.drawLine(p.x,p.y,p.x,p.y);
    }

    private void AgregarLinea(Linea linea) {
        this.Figuras.add(linea);
        Graphics2D g = (Graphics2D) this.jPanel3.getGraphics();
        linea.Dibujar(g);
    }
    
    private void AgregarTriangulo(Point p1, Point p2, Point p3, Color color) {
        Linea l1, l2, l3;
        l1 = new Linea(p1,p2,color);
        l2 = new Linea(p2,p3,color);
        l3 = new Linea(p3,p1,color);
        
        Triangulo triangulo = new Triangulo(l1,l2,l3,color);
                
        this.Figuras.add(triangulo);
        
        Graphics2D g2d = (Graphics2D) this.jPanel3.getGraphics();
        triangulo.Dibujar(g2d);
        System.out.println("Se agrego triangulo");
        
    } 
    
    private void AgregarCuadrado(Cuadrado cuadrado){
        this.Figuras.add(cuadrado);
        Graphics2D g2d = (Graphics2D) this.jPanel3.getGraphics();
        cuadrado.Dibujar(g2d);  
        
    }

    private void AgregarCirculo(Circulo circulo){
        this.Figuras.add(circulo);
        Graphics2D g2d = (Graphics2D) this.jPanel3.getGraphics();
        circulo.Dibujar(g2d);    
    }
    
    private void AgregarOvalo(Ovalo ovalo){
        this.Figuras.add(ovalo);
        Graphics2D g2d = (Graphics2D) this.jPanel3.getGraphics();
        ovalo.Dibujar(g2d);    
    }
    
    private void AgregarTexto(Texto texto){
        this.Figuras.add(texto);
        Graphics2D g2d = (Graphics2D) this.jPanel3.getGraphics();
        texto.Dibujar(g2d);    
    }
    
    void redibujar(){
        Graphics2D g2d = (Graphics2D) this.jPanel3.getGraphics();
        
        Iterator itr = Figuras.iterator();
        int i = 0;
        while(itr.hasNext()){
            Figura f = (Figura)itr.next();
            
            if (f instanceof Linea){
                Linea l = (Linea)f;
                l.Dibujar(g2d);
            }
            
            if (f instanceof Triangulo){
                Triangulo t = (Triangulo)f;
                t.Dibujar(g2d);
            }     
            
            if (f instanceof Cuadrado){
                Cuadrado c = (Cuadrado)f;
                c.Dibujar(g2d);
            }                 
            
            if (f instanceof Circulo){
                Circulo c = (Circulo)f;
                c.Dibujar(g2d);
            }  

            if (f instanceof Ovalo){
                Ovalo c = (Ovalo)f;
                c.Dibujar(g2d);
            }                          
            
            if (f instanceof Texto){
                Texto t = (Texto)f;
                t.Dibujar(g2d);
            }             
        }   
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        tfColor = new javax.swing.JTextField();
        jToggleButton7 = new javax.swing.JToggleButton();
        jToggleButton8 = new javax.swing.JToggleButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jToggleButton1 = new javax.swing.JToggleButton();
        jToggleButton2 = new javax.swing.JToggleButton();
        jToggleButton4 = new javax.swing.JToggleButton();
        jToggleButton5 = new javax.swing.JToggleButton();
        jToggleButton6 = new javax.swing.JToggleButton();
        jTextField1 = new javax.swing.JTextField();
        jToggleButton3 = new javax.swing.JToggleButton();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel3MouseClicked(evt);
            }
        });
        jPanel3.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                jPanel3ComponentResized(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 305, Short.MAX_VALUE)
        );

        jPanel1.setBackground(new java.awt.Color(255, 255, 0));

        jButton1.setText("Borrar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        tfColor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tfColorMouseClicked(evt);
            }
        });

        jToggleButton7.setText("Guardar");
        jToggleButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton7ActionPerformed(evt);
            }
        });

        jToggleButton8.setText("Abrir");
        jToggleButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton8ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tfColor, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 68, Short.MAX_VALUE)
                .addComponent(jToggleButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(68, 68, 68)
                .addComponent(jToggleButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(40, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(tfColor, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jToggleButton7)
                    .addComponent(jToggleButton8))
                .addGap(37, 37, 37))
        );

        jPanel2.setBackground(new java.awt.Color(0, 102, 255));

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("FIGURAS");

        jToggleButton1.setBackground(new java.awt.Color(255, 255, 0));
        buttonGroup1.add(jToggleButton1);
        jToggleButton1.setText("Linea");
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });

        jToggleButton2.setBackground(new java.awt.Color(255, 255, 0));
        buttonGroup1.add(jToggleButton2);
        jToggleButton2.setText("Triangulo");
        jToggleButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton2ActionPerformed(evt);
            }
        });

        jToggleButton4.setBackground(new java.awt.Color(255, 255, 0));
        buttonGroup1.add(jToggleButton4);
        jToggleButton4.setText("Circulo");
        jToggleButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton4ActionPerformed(evt);
            }
        });

        jToggleButton5.setBackground(new java.awt.Color(255, 255, 0));
        buttonGroup1.add(jToggleButton5);
        jToggleButton5.setText("Ovalo");
        jToggleButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton5ActionPerformed(evt);
            }
        });

        jToggleButton6.setBackground(new java.awt.Color(255, 255, 0));
        buttonGroup1.add(jToggleButton6);
        jToggleButton6.setText("Cuadrado");
        jToggleButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton6ActionPerformed(evt);
            }
        });

        jToggleButton3.setBackground(new java.awt.Color(255, 255, 0));
        buttonGroup1.add(jToggleButton3);
        jToggleButton3.setText("Texto");
        jToggleButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton3ActionPerformed(evt);
            }
        });

        jRadioButton1.setBackground(new java.awt.Color(0, 102, 255));
        buttonGroup2.add(jRadioButton1);
        jRadioButton1.setForeground(new java.awt.Color(255, 255, 255));
        jRadioButton1.setText("Relleno");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });

        jRadioButton2.setBackground(new java.awt.Color(0, 102, 255));
        buttonGroup2.add(jRadioButton2);
        jRadioButton2.setForeground(new java.awt.Color(255, 255, 255));
        jRadioButton2.setText("Vacio");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jToggleButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jToggleButton2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jToggleButton4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jToggleButton5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jToggleButton6, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
                    .addComponent(jTextField1)
                    .addComponent(jToggleButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jRadioButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jRadioButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jToggleButton1)
                .addGap(18, 18, 18)
                .addComponent(jToggleButton2)
                .addGap(18, 18, 18)
                .addComponent(jToggleButton6)
                .addGap(18, 18, 18)
                .addComponent(jToggleButton4)
                .addGap(18, 18, 18)
                .addComponent(jToggleButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jToggleButton3)
                .addGap(18, 18, 18)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jRadioButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jRadioButton2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        this.Figuras.clear();
        this.jPanel3.repaint();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed
        this.Figura = Linea;       
    }//GEN-LAST:event_jToggleButton1ActionPerformed

    private void jToggleButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton2ActionPerformed
        this.Figura = Triangulo;
    }//GEN-LAST:event_jToggleButton2ActionPerformed

    private void jToggleButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton4ActionPerformed
        this.Figura = Circulo;
    }//GEN-LAST:event_jToggleButton4ActionPerformed

    private void jToggleButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton5ActionPerformed
        this.Figura = Ovalo;
    }//GEN-LAST:event_jToggleButton5ActionPerformed

    private void tfColorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tfColorMouseClicked
       
        Color NuevoColor;
        NuevoColor=JColorChooser.showDialog(this,"Seleccionar color",this.color); 
        
        if(NuevoColor!=null){
            this.color = NuevoColor;
        } 
        this.tfColor.setBackground(this.color);
    }//GEN-LAST:event_tfColorMouseClicked

    private void jPanel3ComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_jPanel3ComponentResized
     
        redibujar();
              
    }//GEN-LAST:event_jPanel3ComponentResized

    private void jPanel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseClicked

        int x = evt.getX();
        int y = evt.getY();
        
        switch (Figura){
            case Linea:
                if (Estado == this.Esperandopunto1){
                    this.punto1.x = x;
                    this.punto1.y = y;
                    punto2.x = punto1.x+1;
                    punto2.y = punto1.y+1;
                    this.DibujarPunto(punto1,this.color);
                    Estado = this.Esperandopunto2;
                    
                } else {
                    this.punto2.x = x;
                    this.punto2.y = y;
                    AgregarLinea(new Linea(this.punto1, this.punto2,this.color));
                    Estado = this.Esperandopunto1;
                }
                break;
                
            case Triangulo:
                if (Estado == this.Esperandopunto1){
                    this.punto1.x = x;
                    this.punto1.y = y;
                    punto2.x = punto1.x+1;
                    punto2.y = punto1.y+1;
                    System.out.println("Se agrego primer punto");
                    Estado = this.Esperandopunto2;
                    
                } else if (Estado == this.Esperandopunto2){
                    this.punto2.x = x;
                    this.punto2.y = y;
                    System.out.println("Se agrego segundo punto");
                    Estado = this.Esperandopunto3;  
                
                } else if (Estado == this.Esperandopunto3) {
                    this.punto3.x = x;
                    this.punto3.y = y;
                    AgregarTriangulo(this.punto1, this.punto2,this.punto3,this.color);
                    Estado = this.Esperandopunto1;
                    System.out.println("Se agrego tercer punto");
                }
                break;  
                
            case Cuadrado:
                if (Estado == this.Esperandopunto1){
                    this.punto1.x = x;
                    this.punto1.y = y;
                    punto2.x = punto1.x+1;
                    punto2.y = punto1.y+1;
                    System.out.println("Se agrego primer punto");
                    Estado = this.Esperandopunto2;
                } else {
                    this.punto2.x = x;
                    this.punto2.y = y;
                    AgregarCuadrado(new Cuadrado(this.punto1, this.punto2,this.color,relleno));
                    Estado = this.Esperandopunto1;
                    System.out.println("Se agrego cuarto punto");
                }
                
                break;
            case Texto:
                if(Estado == this.Esperandopunto1){
                    this.punto1.x = x;
                    this.punto1.y = y;
                    punto2.x = punto1.x+1;
                    punto2.y = punto1.y+2;
                    System.out.print("Se dibuja punto");
                    
                    Graphics2D g2d = (Graphics2D) this.jPanel3.getGraphics();
                    AgregarTexto(new Texto(this.jTextField1.getText(),this.punto1,this.color));
                    Estado = this.Esperandopunto1;
                    
                }
                break;
            case Circulo:
                if (Estado == this.Esperandopunto1){                    
                    this.punto1.x = x;
                    this.punto1.y = y;
                    punto2.x = punto1.x+1;
                    punto2.y = punto1.y+1;
                    System.out.println("Se dibuja punto");
                    Estado = this.Esperandopunto2;
                } else {
                    this.punto2.x = x;
                    this.punto2.y = y;
                    System.out.println("Se agrega el circulo");
                    AgregarCirculo(new Circulo(this.punto1, Math.abs(this.punto2.x-this.punto1.x),this.color,relleno));
                    Estado = this.Esperandopunto1;
                }
                break;
                
            case Ovalo:
                if (Estado == this.Esperandopunto1){                    
                    this.punto1.x = x;
                    this.punto1.y = y;
                    punto2.x = punto1.x+1;
                    punto2.y = punto1.y+1;
                    Estado = this.Esperandopunto2;
                } else {
                    this.punto2.x = x;
                    this.punto2.y = y;
                    AgregarOvalo(new Ovalo(this.punto1, this.punto2,this.color,relleno));
                    Estado = this.Esperandopunto1;
                }
                break;
        }  
    }//GEN-LAST:event_jPanel3MouseClicked

    private void jToggleButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton6ActionPerformed
        // TODO add your handling code here:
        this.Figura = Cuadrado;
    }//GEN-LAST:event_jToggleButton6ActionPerformed

    private void jToggleButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton3ActionPerformed
        // TODO add your handling code here:
        this.Figura = Texto;
    }//GEN-LAST:event_jToggleButton3ActionPerformed

    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        if(jRadioButton1.isSelected()){
            relleno=true;
        }
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
        if(jRadioButton2.isSelected()){
            relleno=false;
        }
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    private void jToggleButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton7ActionPerformed
        // TODO add your handling code here:
        JFileChooser chooser = new JFileChooser();
        String nombrearchivo="";
        
        int returnval = chooser.showSaveDialog(this);
        if(returnval==JFileChooser.APPROVE_OPTION){
            nombrearchivo=chooser.getSelectedFile().getPath();
          
            
            try{
                FileWriter fw = new FileWriter(nombrearchivo);
                Iterator itr = Figuras.iterator();
                int i = 0;
                Figura f = (Figura)itr.next();
                if(f instanceof Linea){
                    Linea l = (Linea)f;
                    String str = String.format("L,%d,%d,%d,%d,%s\n",l.punto1.x,l.punto1.y,l.punto2.x,l.punto2.y,Integer.toHexString(l.color.getRGB()).substring(2));
                    fw.write(str);
                }if(f instanceof Cuadrado){
                    Cuadrado c = (Cuadrado)f;
                    String str = String.format("K,%d,%d,%d,%d,%s\n",c.punto1.x,c.punto1.y,c.punto2.x,c.punto2.y,Integer.toHexString(c.color.getRGB()).substring(2));
                    fw.write(str);
                }if(f instanceof Triangulo){
                    Triangulo t = (Triangulo)f;
                    String str = String.format("T,%d,%d,%d,%d,%d,%d,%s\n",t.linea1.punto1.x,t.linea1.punto1.y,t.linea1.punto2.x,t.linea1.punto2.y,t.linea2.punto2.x,t.linea2.punto2.y,Integer.toHexString(t.color.getRGB()).substring(2));
                    fw.write(str);
                }if(f instanceof Ovalo){
                    Ovalo c = (Ovalo)f;
                    String str = String.format("O,%d,%d,%d,%d,%s\n",c.punto1.x,c.punto1.y,c.punto2.x,c.punto2.y,Integer.toHexString(c.color.getRGB()).substring(2));
                    fw.write(str);
                }if(f instanceof Texto){
                    Texto t = (Texto)f;
                    String str = String.format("X,%d,%d,%s,%s\n",t.punto1.x,t.punto1.y,t.texto,Integer.toHexString(t.color.getRGB()).substring(2));
                    fw.write(str);
                }if(f instanceof Circulo){
                    Circulo c = (Circulo)f;
                    String str = String.format("C,%d,%d,%d,%s\n",c.centro.x,c.centro.y,c.radio,Integer.toHexString(c.color.getRGB()).substring(2));
                    fw.write(str);
                }
                
                
                fw.close();
        }catch(IOException ex){
            Logger.getLogger(PracticaNo8.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
        
        
    }//GEN-LAST:event_jToggleButton7ActionPerformed

    private void jToggleButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton8ActionPerformed

        this.Figuras.clear();
        JFileChooser chooser = new JFileChooser();
        String nombrearchivo="";
        int returnval = chooser.showOpenDialog(this);
        if(returnval == JFileChooser.APPROVE_OPTION){
            nombrearchivo=chooser.getSelectedFile().getPath();
            try{
                FileReader a = new FileReader(nombrearchivo);
                BufferedReader b = new BufferedReader(a);
                String linea = "";
                
                while((linea=b.readLine())!=null){
                    if(linea.startsWith("L")){
                        this.Figuras.add(new Linea(linea));
                    }
                    if(linea.startsWith("T")){
                        this.Figuras.add(new Triangulo(linea));
                    }
                    if(linea.startsWith("K")){
                        this.Figuras.add(new Cuadrado(linea));
                    }
                    if(linea.startsWith("C")){
                        this.Figuras.add(new Circulo(linea));
                    }
                    if(linea.startsWith("O")){
                        this.Figuras.add(new Ovalo(linea));
                    }
                    if(linea.startsWith("X")){
                        this.Figuras.add(new Texto(linea));
                    }
                }
            }catch(IOException ex){
                Logger.getLogger(PracticaNo8.class.getName()).log(Level.SEVERE,null,ex);
                
            }
            this.redibujar();
        }
       
        
    }//GEN-LAST:event_jToggleButton8ActionPerformed

    public static void main(String args[]) {
 
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PracticaNo8().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JToggleButton jToggleButton2;
    private javax.swing.JToggleButton jToggleButton3;
    private javax.swing.JToggleButton jToggleButton4;
    private javax.swing.JToggleButton jToggleButton5;
    private javax.swing.JToggleButton jToggleButton6;
    private javax.swing.JToggleButton jToggleButton7;
    private javax.swing.JToggleButton jToggleButton8;
    private javax.swing.JTextField tfColor;
    // End of variables declaration//GEN-END:variables
}
