
package MiniCad;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

public class Cuadrado extends Figura {
    
    Point punto1;
    Point punto2;
    Color color;
    boolean relleno;

    public Cuadrado(Point _punto1,Point _punto2, Color _color,boolean _relleno) {
        punto1 = new Point(_punto1.x,_punto1.y);
        punto2 = new Point(_punto2.x,_punto2.y);
        color  = _color;
        this.color = color;
        relleno = _relleno;
    }

    Cuadrado(String linea) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void Dibujar(Graphics2D g2d){
        if (this.color!=null){
            g2d.setColor(color);
        } 
        if(relleno==false)
        {
        g2d.drawLine(punto1.x, punto1.y,punto2.x,punto1.y);
        g2d.drawLine(punto2.x, punto1.y,punto2.x,punto2.y);
        g2d.drawLine(punto1.x, punto2.y,punto2.x,punto2.y);
        g2d.drawLine(punto1.x, punto1.y,punto1.x,punto2.y);
        }else{
            g2d.fillRect(punto1.x, punto1.y, punto2.y, punto2.x);
        }
    }    
}
